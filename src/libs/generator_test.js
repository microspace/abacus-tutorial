utils = require('./generator');

function process(cfg) {
  res = utils.generate(cfg);
  console.log('Level:', cfg.Level, '| Operation:', cfg.Operation, '| Result:', res);
  seq_sum = res.seq.reduce(function (a, b) { return a + b; });
  ans = res.answer.reverse().reduce(function (a, b) { return a * 10 + b});
  console.assert(seq_sum == ans, seq_sum, ans);
  console.assert(res.seq.length == cfg.Kolslog);
}

levels = [
  { level: 1, Checked_cifr: [1, 2, 3, 4, 6, 7, 8, 9]},
  { level: 2, Checked_cifr: [1, 2, 3, 4]},
  { level: 3, Checked_cifr: [1, 2, 3, 4, 5, 6, 7, 8, 9]},
  { level: 4, Checked_cifr: [6, 7, 8, 9]},
  { level: 5, Checked_cifr: [1, 2, 3, 4, 5, 6, 7, 8, 9]}
];

levels.forEach(function(item) {
  var cfg = {
    Checked_cifr: item.Checked_cifr,
    Level: item.level,
    Operation: 2,
    Kolslog: 4,
    Kolcifr: 2
  }

  // console.log(utils.possible_checked(cfg.Level, cfg.Operation));

  // process(cfg);
});

var times = 10;
var level = { level: 2, Checked_cifr: [1]};
for (var i = 0; i < 10; i++) {
  var cfg = {
    Checked_cifr: level.Checked_cifr,
    Level: level.level,
    Operation: 2,
    Kolslog: 3,
    Kolcifr: 1
  }

  process(cfg);
}