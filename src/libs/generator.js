var utils = {
	random: function (min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	},
	cloneObject: function (obj) {
		return JSON.parse(JSON.stringify(obj));
	},
	debounce: function (func, interval) {
        let lastCall = -1;
        return function() {
            clearTimeout(lastCall);
            let args = arguments;
            let self = this;
            lastCall = setTimeout(function() {
                func.apply(self, args);
            }, interval);
        };
    },
	throttle: function (func, interval) {
        let lastCall = 0;
        return function() {
            let now = Date.now();
            if (lastCall + interval < now) {
                lastCall = now;
                return func.apply(this, arguments);
            }
        };
    }
};

let _utils = (function () {
    const POSSIBLE_CONST_PROSTO = [
        [
            [
                [0,1,2,3,4,5,6,7,8,9],
                [0,1,2,3,5,6,7,8],
                [0,1,2,5,6,7],
                [0,1,5,6],[0,5],
                [0,1,2,3,4],
                [0,1,2,3],
                [0,1,2],
                [0,1],
                [0]
            ],
            [
                [],[],[],[],[],[],[],[],[],[]
            ]
        ],
        [
            [
                [],[],[],[],[],[],[],[],[],[]
            ],
            [
                [0],
                [0,1],
                [0,1,2],
                [0,1,2,3],
                [0,1,2,3,4],
                [0,5],
                [0,1,5,6],
                [0,1,2,5,6,7],
                [0,1,2,3,5,6,7,8],
                [0,1,2,3,4,5,6,7,8,9]
            ]
        ],
        [
            [
                [0,1,2,3,4,5,6,7,8,9],
                [0,1,2,3,5,6,7,8],
                [0,1,2,5,6,7],
                [0,1,5,6],
                [0,5],
                [0,1,2,3,4],
                [0,1,2,3],
                [0,1,2],
                [0,1],
                [0]
            ],
            [
                [0],
                [0,1],
                [0,1,2],
                [0,1,2,3],
                [0,1,2,3,4],
                [0,5],
                [0,1,5,6],
                [0,1,2,5,6,7],
                [0,1,2,3,5,6,7,8],
                [0,1,2,3,4,5,6,7,8,9]
            ]
        ]
    ];
    const POSSIBLE_CONST_BRAT = [
        [
            [
                [1,2,3,4],
                [4],
                [3,4],
                [2,3,4],
                [1,2,3,4],
                [0,1,2,3,4],
                [0,1,2,3],
                [0,1,2],
                [0,1],
                [0]
            ],
            [
                [0],
                [0,1],
                [0,1,2],
                [0,1,2,3],
                [0,1,2,3,4],
                [0,5],
                [5],
                [5,6],
                [5,6,7],
                [5,6,7,8]
            ]
        ],
        [
            [
                [5,6,7,8],
                [5,6,7,8],
                [5,6,7],
                [5,6],
                [0,5],
                [0,1,2,3],
                [0,1,2],
                [0,1],
                [0,1],
                [0]
            ],
            [
                [0],
                [0,1],
                [0,1,2],
                [0,1,2,3],
                [0,1,2,3,4],
                [1,2,3,4],
                [2,3,4],
                [3,4],
                [4],
                [1,2,3,4]
            ]
        ],
        [
            [
                [1,2,3,4],
                [4],
                [3,4],
                [2,3,4],
                [1,2,3,4],
                [0,1,2,3,4],
                [0,1,2,3],
                [0,1,2],
                [0,1],
                [0]
            ],
            [
                [0],
                [0,1],
                [0,1,2],
                [0,1,2,3],
                [0,1,2,3,4],
                [1,2,3,4],
                [2,3,4],
                [3,4],
                [4],
                [0,1,2,3,4,5,6,7,8,9]
            ]
        ]
    ];
    const POSSIBLE_CONST_DRUG =[
        [
            [
                [3,4,5,6,7],
                [4,9],
                [4,3,8,9],
                [4,3,2,7,8,9],
                [6,7,8,9],
                [5],
                [4,5,9],
                [3,4,5,8,9],
                [2,3,4,5,7,8,9],
                [1,2,3,4,5,6,7,8,9]
            ],
            [
                [],[],[],[],[],[],[],[],[],[]]
        ],
        [
            [[],[],[],[],[],[],[],[],[],[]
            ],
            [
                [1,2,3,4,5,6,7,8,9],
                [2,3,4,5,7,8,9],
                [3,4,5,8,9],
                [4,5,9],
                [5],
                [6,7,8,9],
                [4,3,2,7,8,9],
                [4,3,8,9],
                [4,9],
                [3,4,5,6,7]
            ]
        ],
        [
            [
                [3,4,5,6,7],
                [4,9],
                [4,3,8,9],
                [4,3,2,7,8,9],
                [6,7,8,9],
                [5],
                [4,5,9],
                [3,4,5,8,9],
                [2,3,4,5,7,8,9],
                [1,2,3,4,5,6,7,8,9]
            ],
            [
                [1,2,3,4,5,6,7,8,9],
                [2,3,4,5,7,8,9],
                [3,4,5,8,9],
                [4,5,9],
                [5],
                [6,7,8,9],
                [4,3,2,7,8,9],
                [4,3,8,9],
                [4,9],
                [3,4,5,6,7]
            ]
        ]
    ];
    const POSSIBLE_CONST_COMBO = [
        [
            [
                [5,6,7,8],
                [5,6,7],
                [3,4,5,6],
                [2,3,4,5],
                [6,7,8,9],
                [6,7,8,9],
                [6,7,8],
                [6,7],
                [6,7,8],
                [6,7,8,9]
            ],
            [
                [],[],[],[],[],[],[],[],[],[]
            ]
        ],
        [
            [
                [],[],[],[],[],[],[],[],[],[]
            ],
            [
                [6,7,8,9],
                [6,7,8],
                [6,7,8,9],
                [6,7,8],
                [6,7,8,9],
                [1,2,3],
                [1,2,3,4],
                [1,2,3,4,5],
                [1,2,3,4,5,6],
                [5,6,7,8]
            ]
        ],
        [
            [
                [1,2,3,4,5,6,7,8],
                [5,6,7,9],
                [3,4,5,6],
                [2,3,4,5],
                [6,7,8,9],
                [6,7,8,9],
                [6,7,8],
                [6,7],
                [6,7,8],
                [6,7,8,9]
            ],
            [
                [6,7,8,9],
                [6,7,8],
                [6,7,8,9],
                [6,7,8],
                [6,7,8,9],
                [1,2,3],
                [1,2,3,4],
                [1,2,3,4,5],
                [1,2,3,4,5,6],
                [1,2,3,4,5,6,7,8]
            ]
        ]
    ];
    let stop_cycle;
    // let primer_num;
    // let primer_num_n;
    let primer;
    var chislo = 0,
        chislo_plus_all = 0,
        znak = 0,
        schotchik = 0,
        Kolslog = 4, // Количество слагаемых
        Kolcifr = 1, // Сложность: 1: 1, 2: 10, 3: 100, 4: 1000
        // Kolcifr2 = null,
        cifr = [],
        Level_Operation = 12, // Level*10 + Operation
        Level = 1, // 1: Просто, 2: Брат, 3: Друг, 4: Комбо, 5: Произвольный
        Operation = 2, // 0: плюс, 1: минус, 2: плюс/минус
        possible_checked_var = [],
        chislo_plus = [],
        // vip = 0, // ??? Количество примеров
        // prav = 0, // Количество правильно решенных примеров
        half = true, // Отвечает за 1/2
        Checked_cifr = [];

    /*
     * @public
     * Функция генерации
     * {Object} cfg конфиг для генерации
     *  {Array} Checked_cifr
     *  {Number} Level Уровень игры - 1: Просто, 2: Брат, 3: Друг, 4: Комбо, 5: Произвольный
     *  {Number} Operation Доступные операции - 0: плюс, 1: минус, 2: плюс/минус
     *  {Number} operationLevel Уровень игры - renamed Level_Operation = Level * 10 + Operation;
     *  {Number} Kolslog Количество слагаемых
     *  {Number} Kolcifr Количество цифр
     *  {Boolean} Half 1/2
     */
    function generate(cfg){
        Level = cfg.Level;
        Operation = cfg.Operation;
        Level_Operation = Level * 10 + Operation;
        if (Array.isArray(cfg.Kolcifr)) {
            Kolcifr = cfg.Kolcifr[0];
            // Kolcifr2 = cfg.Kolcifr[1];
        } else {
            Kolcifr = cfg.Kolcifr;
        }
        Kolslog = cfg.Kolslog;
        Checked_cifr = cfg.Checked_cifr;
        half = cfg.Half;

        stop_cycle = false
        schotchik = 0
        chislo = 0;
        cifr = [];
        chislo_plus = [];
        // primer_num=0
        // primer_num_n = [0,0,0,0,0,0,0,0,0]
         primer = []
let i;
        for (i = 0; i < Kolcifr ; i++) {
            let t =  possible_checked(Level,Operation);
            possible_checked_var[i] = t.slice();
        }

        switch (parseInt(Level_Operation)) {
            case 10: {
                znak = 0;
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr-1; i++){
                    cifr[i] = randomInteger(4);
                    chislo += cifr[i]*power10(i);
                }
                 i = Kolcifr - 1;
                cifr[i] = 1 + randomInteger(3);
                chislo += cifr[i]*power10(i);
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_simple_add()
                }

            }
                break;

            case 11: {
                chislo = 0;
                let i;
                let t = Checked_cifr.slice();
                exclude(0,t);
                for ( i = 0; i < Kolcifr; i++){
                    cifr[i] = t[randomInteger(t.length)]
                    chislo += cifr[i]*power10(i);
                }
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_simple_sub()
                }

            }
                break;

            case 12: {
                chislo = 0;
                let i;
                let t = Checked_cifr.slice();
                for ( i = 0; i < Kolcifr-1; i++){
                    cifr[i] = t[randomInteger(t.length)]
                    chislo += cifr[i]*power10(i);
                }
                exclude(0,t);
                i = Kolcifr - 1;
                cifr[i] = t[randomInteger(t.length)]
                chislo += cifr[i]*power10(i);

                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_simple_add_sub()
                }

            }
                break;

            case 20: {
                znak = 0;
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr-1; i++){
                    cifr[i] = randomInteger(5);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr - 1;
                cifr[i] = 2 + randomInteger(3);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_brother_add()
                }

            }
                break;

            case 21: {
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr; i++){
                    cifr[i] = 5+randomInteger(5);
                    chislo += cifr[i]*power10(i);
                }
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_brother_sub()
                }
            }
                break;

            case 22: {
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr-1; i++){
                    cifr[i] = randomInteger(10);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr - 1;
                cifr[i] = 2 + randomInteger(8);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_brother_add_sub()
                }

            }
                break;

            case 30: {
                znak = 0;
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr-1; i++){
                    cifr[i] = randomInteger(10);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr - 1;
                cifr[i] = 2 + randomInteger(8);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_friend_add()
                }

            }
                break;

            case 31: {
                znak = 1;
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr; i++){
                    cifr[i] = randomInteger(10);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr;
                cifr[i] = 2 + randomInteger(8);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_friend_sub()
                }

            }
                break;

            case 32: {
                znak = 0;
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr-1; i++){
                    cifr[i] = randomInteger(10);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr - 1;
                cifr[i] = 2 + randomInteger(8);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_friend_add_sub()
                }

            }
                break;

            case 40: {
                znak = 0;
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr-1; i++){
                    cifr[i] = randomInteger(10);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr - 1;
                cifr[i] = 2 + randomInteger(8);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_combo_add()
                }

            }
                break;

            case 41: {
                znak = 1;
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr; i++){
                    cifr[i] = randomInteger(10);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr;
                cifr[i] = 2 + randomInteger(8);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_combo_sub()
                }

            }
                break;

            case 42: {
                znak = 0;
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr-1; i++){
                    cifr[i] = randomInteger(10);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr - 1;
                cifr[i] = 2 + randomInteger(8);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_combo_add_sub()
                }

            }
                break;
            case 50: {
                znak = 0;
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr-1; i++){
                    cifr[i] = randomInteger(10);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr - 1;
                cifr[i] = 2 + randomInteger(8);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_random_add()
                }

            }
                break;

            case 51: {
                znak = 1;
                chislo = 0;
                let i;
                for ( i = 0; i < Kolcifr; i++){
                    cifr[i] = randomInteger(10);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr;
                cifr[i] = 2 + randomInteger(8);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_random_sub()
                }

            }
                break;

            case 52: {
                znak = 0;
                chislo = 0;
                let i;
                for (i = 0; i < Kolcifr-1; i++){
                    cifr[i] = randomInteger(10);
                    chislo += cifr[i]*power10(i);
                }
                i = Kolcifr - 1;
                cifr[i] = 2 + randomInteger(8);
                chislo += cifr[i]*power10(i);
                ;
                primer.push(chislo)
                while (stop_cycle === false){
                    calculate_random_add_sub()
                }

            }
                break;
            default:
                break;
        }

        return {
            seq: primer,
            // answer: cifr // something wrong with cifr for level 3+
            answer: splitAndReverse(primer.reduce(function (a, b) { return a + b; }))
        };
    }

    function generateFractional(cfg) {
        let result = {
            seq: [],
            newAnswer: 0
        };
        let {
            digitsCountAfterPoint,
            digitsCountBeforePoint,
            termsCount,
            operations,
            treatZero
        } = cfg;
        operations = [
            "plus",
            "plusMinus"
        ].indexOf(operations) > -1 ? operations : "plus";
        if (operations === "plus") {
            for (let i = 0, l = termsCount; i < l; i++) {
                let before = utils.random(1, 9) + "";
                let after = utils.random(0, 9) + "";
                for (let i = 1, l = digitsCountBeforePoint; i < l; i++) {
                    before += utils.random(0, 9);
                }
                for (let i = 1, l = digitsCountAfterPoint; i < l; i++) {
                    after += utils.random(0, 9);
                }
                result.seq.push(before + "." + after);
                result.newAnswer += (before + "." + after)*1;
                result.newAnswer = result.newAnswer.toFixed(digitsCountAfterPoint)*1;
            }
        } else {
            if (treatZero === "around") {
                generateAroundZero();
            } else if (treatZero === "more") {
                generate(1);
            } else if (treatZero === "less") {
                generate(-1);
            } else {
                generateAroundZero();
            }
        }
        return result;

        function generateAroundZero() {
            let a = 1;
            for (let i = 0, l = termsCount; i < l; i++) {
                let before = utils.random(1, result.newAnswer === 0 ? 6 : 9) + "";
                let after = utils.random(0, 9) + "";
                let prenum;
                for (let i = 1, l = digitsCountBeforePoint; i < l; i++) {
                    before += utils.random(0, 9);
                }
                for (let i = 1, l = digitsCountAfterPoint; i < l; i++) {
                    after += utils.random(0, 9);
                }
                prenum = (before + "." + after)*1;
                while (!prenum || (a === 1 ? result.newAnswer + prenum < 0 : result.newAnswer - prenum > 0)) {
                    let before2 = utils.random(before[0]*1, 9) + "";
                    let after2 = utils.random(0, 9) + "";
                    for (let i = 1, l = digitsCountBeforePoint; i < l; i++) {
                        before2 += utils.random(0, 9);
                    }
                    for (let i = 1, l = digitsCountAfterPoint; i < l; i++) {
                        after2 += utils.random(0, 9);
                    }
                    prenum = (before2 + "." + after2)*1;
                }
                prenum *= a;
                result.seq.push(prenum.toFixed(digitsCountAfterPoint).replace(".", "."));
                result.newAnswer += prenum;
                result.newAnswer = result.newAnswer.toFixed(digitsCountAfterPoint)*1;
                a *= -1;
            }
        }

        function generate(b) {
            b = b*1 > 0 ? 1 : -1;
            console.log(b);
            let reachedMinus = false;
            let limit = Math.pow(10, digitsCountBeforePoint)*2.5;
            for (let i = 0, l = termsCount; i < l; i++) {
                let before = utils.random(1, 9) + "";
                let after = utils.random(0, 9) + "";
                let prenum;
                for (let i = 1, l = digitsCountBeforePoint; i < l; i++) {
                    before += utils.random(0, 9);
                }
                for (let i = 1, l = digitsCountAfterPoint; i < l; i++) {
                    after += utils.random(0, 9);
                }
                prenum = (before + "." + after)*1;
                let a = (result.newAnswer !== 0 && utils.random(0, 10) > 3) || i === 1 ? -1 : 1;
                prenum *= a;
                let ii = 0;
                reachedMinus = reachedMinus || result.newAnswer + prenum < 0;
                while ( (!prenum || (
                    b === -1 ?
                        reachedMinus ?
                            result.newAnswer + prenum > 0 :
                            false :
                        result.newAnswer + prenum < 0
                )) && !!result.newAnswer) {

                    let before2 = utils.random(1, 9) + "";
                    let after2 = utils.random(0, 9) + "";
                    for (let i = 1, l = digitsCountBeforePoint; i < l; i++) {
                        before2 += utils.random(0, 9);
                    }
                    for (let i = 1, l = digitsCountAfterPoint; i < l; i++) {
                        after2 += utils.random(0, 9);
                    }
                    prenum = (before2 + "." + after2)*1;
                    prenum *= a;
                    if (ii > 3) {
                        prenum *= -1;
                        break;
                    }
                    ii++;
                }
                if (Math.abs(result.newAnswer + prenum) > limit)
                    prenum *= -1;
                result.seq.push(prenum.toFixed(digitsCountAfterPoint).replace(".", "."));
                result.newAnswer += prenum;
                result.newAnswer = result.newAnswer.toFixed(digitsCountAfterPoint)*1;
            }
        }
    }

    /*
     * @private
     * Превращает число в перевернутый массив (нужно для ответа генератора)
     * {Number} n число
     * return {Array of Number}
     */
    function splitAndReverse(n) {
        return Array.from(n.toString())
                    .map(function(digit) {
                        return parseInt(digit)
                    })
                    .reverse();
    }

    /*
     * @private
     * Генерирует рандомное число
     * {Number} n число
     * return {Number}
     */
    function randomInteger (n, m) {
        let result;
        if (m) {
            result = utils.random(n, m);
        } else {
            result = Math.floor(Math.random()*(n-0.0001));
        }
        // console.log(n);
        // console.log(result);
        // if (m) {
        //     return utils.random(n, m);
        // } else {
        //     return Math.floor(Math.random()*(n-0.0001));
        // }
        return result;
    }

    /*
     * @private
     * {Number} n число
     * return {Number}
     */
    function power10(n){
        return Math.pow(10, n);
    }

    /*
     * @private
     */
    function duplicat(b, c) {
        var d = [],
            e = {},
            f = {};

        for (let a = 0; a < b.length; a++) {
            e[b[a]] = !0;
        }

        for (let a = 0; a < c.length; a++) {
            f[c[a]] = !0;
        }

        for (var g in e) {
            f[g] && d.push(g);
        }

        return d;
    };

    /*
     * @private
     */
    function ravno(number) {
        stop_cycle = true;
    }

    /*
     * @private
     */
    function chislo_plus_f(){
        chislo += chislo_plus_all;
        primer.push(chislo_plus_all)
        chislo = parseInt(chislo);
        chislo_plus_all = parseInt(chislo_plus_all);
    }

    /*
     * @private
     */
    function chislo_minus_f(){
        chislo -= chislo_plus_all;
        let ch = -chislo_plus_all
        primer.push(ch)
        chislo = parseInt(chislo);
        chislo_plus_all = parseInt(chislo_plus_all);
    }

    /*
     * @private
     */
/*     function include(a, arr) {
        if (arr.indexOf(a) === -1) {
            arr.push(a);
        }

        return arr;
    }; */

    /*
     * @private
     */
    function exclude(a, arr) {
        let index = arr.indexOf(a);

        if (index !== -1) {
            arr.splice(index, 1);
        }

        return arr;
    }

    /*
     * @private
     */
    function yes_no(a, arr){
        var c = false, i = 0;
        while (c === false && i<arr.length) {
            if (a === arr[i++]) c = true;
        }
        return c;
    }

    /*
     * @private
     * {Number} n Level
     * {Number} m Operation
     * return {Number} possible numbers
     */
    function possible_checked (n,m){
        let arrchecked = Checked_cifr.slice();
        let initial =
            [[[0,1,2,3,4,5,6,7,8,9],[0,1,2,3,5,6,7,8],[0,1,2,5,6,7],[0,1,5,6],[0,5],[0,1,2,3,4],[0,1,2,3],[0,1,2],[0,1],[0]],
                [[0],[0,1],[0,1,2],[0,1,2,3],[0,1,2,3,4],[0,5],[0,1,5,6],[0,1,2,5,6,7],[0,1,2,3,5,6,7,8],[0,1,2,3,4,5,6,7,8,9]]];
        let arr_possible = [[[],[],[],[],[],[],[],[],[],[]],
            [[],[],[],[],[],[],[],[],[],[]]];
        if (n === 2){
            for (let j = 0; j <= 1; j++){
                for (let k = 0; k <= 9; k++){
                    arr_possible[j][k] = initial[j][k].slice();
                }
            }
        }
        switch (parseInt(n)) {
            case 1:
                for (let j = 0; j <= 1; j++){
                    for (let k = 0; k <= 9; k++){
                        arr_possible[j][k] = duplicat(arrchecked, POSSIBLE_CONST_PROSTO[m][j][k]);
                        if (arr_possible[j][k].length === 0){
                            arr_possible[j][k] = initial[j][k].slice();
                        }
                    }
                }
                break;
            case 2:
                for (let j = 0; j <= 1; j++){
                    for (let k = 0; k <= 9; k++){
                        arr_possible[j][k] = POSSIBLE_CONST_BRAT[m][j][k].slice();
                    }
                }
                switch (parseInt(m)){
                    case 0:
                        for (let k = 1; k <= 4; k++){
                            arr_possible[0][k] = duplicat(arrchecked, POSSIBLE_CONST_BRAT[m][0][k]);
                            if (arr_possible[0][k].length === 0 || arr_possible[0][k] === 0){
                                arr_possible[0][k] = initial[0][k].slice();
                            }
                        }
                        break;
                    case 1:
                        for (let k = 5; k <= 8; k++){
                            arr_possible[1][k] = duplicat(arrchecked, POSSIBLE_CONST_BRAT[m][1][k]);
                            if (arr_possible[1][k].length === 0 || arr_possible[1][k] === 0){
                                arr_possible[1][k] = initial[1][k].slice();
                            }
                        }
                        break;
                    case 2:
                        for (let k = 1; k <= 4; k++){
                            arr_possible[0][k] = duplicat(arrchecked, POSSIBLE_CONST_BRAT[m][0][k]);
                            if (arr_possible[0][k].length === 0 || arr_possible[0][k] === 0){
                              arr_possible[0][k] = initial[0][k].slice();
                            }
                        }
                        for (let k = 5; k <= 8; k++){
                            arr_possible[1][k] = duplicat(arrchecked, POSSIBLE_CONST_BRAT[m][1][k]);
                            if (arr_possible[1][k].length === 0 || arr_possible[1][k] === 0){
                              arr_possible[1][k] = initial[1][k].slice();
                            }
                        }
                      break;
                    default:
                        break;
                }
                break;
            case 3:
                var r = 1, p = 1;
                switch (parseInt(m)) {
                    case 0: r = 0; p = 0; break;
                    case 1: r = 1; p = 1; break;
                    case 2: r = 0; p = 1; break;
                    default: break;
                }
                for (let j = r; j <= p; j++){
                    for (let k = 0; k <= 9; k++){
                        arr_possible[j][k] = duplicat(arrchecked, POSSIBLE_CONST_DRUG[m][j][k]);
                        if (arr_possible[j][k].length === 0){
                            arr_possible[j][k] = initial[j][k].slice();
                        }
                    }
                }
                break;
            case 4:
                // var r = 1, p = 1;
                switch (parseInt(m)) {
                    case 0:
                        for (let k = 0; k <= 4; k++){
                            arr_possible[0][k] = POSSIBLE_CONST_COMBO[0][0][k].slice();
                        }
                        for (let k = 5; k <= 9; k++){
                            arr_possible[0][k] = duplicat(arrchecked, POSSIBLE_CONST_COMBO[0][0][k]);
                            if (arr_possible[0][k].length === 0){
                                arr_possible[0][k] =  duplicat([6,7,8,9], POSSIBLE_CONST_DRUG[2][0][k]);
                            }
                            if (arr_possible[0][k].length === 0){
                                arr_possible[0][k] =  initial[0][k].slice();
                            }
                        }
                        break;
                    case 1:
                        for (let k = 6; k <= 9; k++){
                            arr_possible[1][k] = POSSIBLE_CONST_COMBO[1][1][k].slice();
                        }
                        for (let k = 0; k <= 5; k++){
                            arr_possible[1][k] = duplicat(arrchecked, POSSIBLE_CONST_COMBO[1][1][k]);
                            if (arr_possible[1][k].length === 0 /*|| arr_possible[j][k] === 0*/){
                                arr_possible[1][k] =  duplicat([6,7,8,9],POSSIBLE_CONST_DRUG[2][1][k]);
                            }
                            if (arr_possible[1][k].length === 0){
                                arr_possible[1][k] =  initial[1][k].slice();
                            }
                        }
                        break;
                    case 2:
                        for (let k = 0; k <= 4; k++){
                            arr_possible[0][k] = POSSIBLE_CONST_COMBO[0][0][k].slice();
                        }
                        for (let k = 5; k <= 9; k++){
                            arr_possible[0][k] = duplicat(arrchecked, POSSIBLE_CONST_COMBO[0][0][k]);
                            if (arr_possible[0][k].length === 0){
                                arr_possible[0][k] =  duplicat([6,7,8,9], POSSIBLE_CONST_DRUG[2][0][k]);
                            }
                            if (arr_possible[0][k].length === 0){
                                arr_possible[0][k] =  initial[0][k].slice();
                            }
                        }
                        for (let k = 6; k <= 9; k++){
                            arr_possible[1][k] = POSSIBLE_CONST_COMBO[1][1][k].slice();
                        }
                        for (let k = 0; k <= 5; k++){
                            arr_possible[1][k] = duplicat(arrchecked, POSSIBLE_CONST_COMBO[1][1][k]);
                            if (arr_possible[1][k].length === 0 /*|| arr_possible[j][k] === 0*/){
                                arr_possible[1][k] =  duplicat([6,7,8,9],POSSIBLE_CONST_DRUG[2][1][k]);
                            }
                            if (arr_possible[1][k].length === 0){
                                arr_possible[1][k] =  initial[1][k].slice();
                            }
                        }


                        break;
                    default:
                        break;
                }
                break;

            case 5:
                for (let i = 0; i <= 9; i++) {
                    for (let j = 0; j<=1; j++){
                        arr_possible[j][i] = [1,2,3,4,5,6,7,8,9];
                    }
                }
                break;
            default:
                break;
        }
        return arr_possible;
    }

    /*
     * @private
     */
    function calculate_simple_add() {
        var max_chislo = 0;
        if (yes_no(5,Checked_cifr)) {
            for (let i = 0; i < Kolcifr; i++){
                max_chislo += 9*power10(i);
            }
        }
        else{
            for (let i = 0; i < Kolcifr; i++){
                max_chislo += 4*power10(i);
            }
        }
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);

        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;

        if (chislo === max_chislo || schotchik === Kolslog) {
            ravno();
        }
        else {
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                for (let i = 0; i < Kolcifr; i++) {
                    let p = possible_checked_var[i][znak][cifr[i]].slice();
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                        p = possible_checked_var[i][znak][cifr[i]].slice();
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < Kolcifr; i++) {
                cifr[i] += parseInt(chislo_plus[i]);
                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }
            chislo_plus_f();
        }
    }

    /*
 * @private
 */
    function calculate_simple_sub() {
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);

        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;

        if (chislo === 0 || schotchik === Kolslog) {
            ravno();
        }
        else {
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                for (let i = 0; i < Kolcifr; i++) {
                    let p = possible_checked_var[i][1][cifr[i]];
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][1][cifr[i]] = POSSIBLE_CHECKED_CONST[1][cifr[i]].slice();
                        p = possible_checked_var[i][1][cifr[i]];
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < Kolcifr; i++) {
                cifr[i] -= parseInt(chislo_plus[i]);
                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }
            chislo_minus_f();
        }
    }

    /*
     * @private
     */
    function calculate_simple_add_sub() {
        chislo = parseInt(chislo);
        var max_chislo_4 = 0, l = Checked_cifr.length-1;
        if (l === 4){
            for (let i = 0; i < Kolcifr; i++){
                max_chislo_4 += 4*power10(i);
            }
        }
        var max_chislo = power10(Kolcifr) - 1;
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);
        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;
        if (schotchik === Kolslog) {
            ravno();
        }
        else {
            var vspom_chislo = 0;
            for (let i = 0; i < Kolcifr; i++){
                vspom_chislo += 5*power10(i);
            }

            switch (chislo){
                case 0: znak = 0; break;
                case vspom_chislo: znak = 0; break;
                case max_chislo: znak = 1; break;
                case max_chislo_4:
                    if ( l === 4 ) znak = 1; else znak = randomInteger(2); break;
                default: znak = randomInteger(2); break;
            }
            var razmer = parseInt((chislo.toString()).length);
            let per = 0;
            if (half === true && razmer > 1 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
                // var uio = chislo%power10(Kolcifr-1);
                switch (chislo%power10(Kolcifr-1)){
                    case 0: znak = 0; break;
                    case parseInt(vspom_chislo/10): znak = 0; break;
                    case parseInt(max_chislo/10): znak = 1; break;
                    case parseInt(max_chislo_4/10):
                        if ( l === 4 ) znak = 1; else znak = randomInteger(2); break;
                    default: znak = randomInteger(2); break;
                }
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = [];

                for (let i = 0; i < Kolcifr - per; i++) {
                    let p = possible_checked_var[i][znak][cifr[i]].slice();
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                        p = possible_checked_var[i][znak][cifr[i]].slice();
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);
            for (let i = 0; i < Kolcifr-per; i++) {
                if (znak === 0)  cifr[i] += parseInt(chislo_plus[i]);
                else cifr[i] -= parseInt(chislo_plus[i]);
                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }

            if (znak === 0) {
                chislo_plus_f();
            }
            else {
                chislo_minus_f();
            }
        }
    }

    /*
     * @private
     */
    function calculate_brother_add() {
        var max_chislo = power10(Kolcifr)-1;
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);

        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;

        if (schotchik === Kolslog) {
            ravno();
        }
        else {
            var vspom_chislo1 = 0, vspom_chislo2 = 0;
            for (let i = 0; i < Kolcifr; i++){
                vspom_chislo1 += 5*power10(i);
                vspom_chislo2 += 7*power10(i);
            }
            if (chislo >= vspom_chislo2) znak = 1;
            else if (chislo < vspom_chislo1) znak = 0;
            else znak = randomInteger(2);
            var razmer = parseInt((chislo.toString()).length);
            let per = 0;
            if (half === true && razmer > 1 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
                switch (chislo%power10(Kolcifr-1)){
                    case 0: znak = 0; break;
                    case parseInt(max_chislo/10): znak = 1; break;
                    default: znak = randomInteger(2); break;
                }
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                for (let i = 0; i < Kolcifr-per; i++) {
                    let p = possible_checked_var[i][znak][cifr[i]].slice();
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                        p = possible_checked_var[i][znak][cifr[i]].slice();
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < Kolcifr-per; i++) {
                if (znak === 0)  cifr[i] += parseInt(chislo_plus[i]);
                else cifr[i] -= parseInt(chislo_plus[i]);
                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }

            if (znak === 0) {
                chislo_plus_f();
            }
            else {
                chislo_minus_f();
            }
        }
    }

    /*
     * @private
     */
    function calculate_brother_sub() {
        var max_chislo = power10(Kolcifr)-1;
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);

        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;

        if (schotchik === Kolslog) {
            ravno();
        }
        else {
            var vspom_chislo1 = 0;
            for (let i = 0; i < Kolcifr; i++){
                vspom_chislo1 += 5*power10(i);
            }
            if (chislo < vspom_chislo1) znak = 0;
            else znak = 1;
            var razmer = parseInt((chislo.toString()).length);
            let per = 0;
            if (half === true && razmer > 1 && Kolcifr > 1 &&  schotchik % 2 === 1){
                per = 1;
                switch (chislo%power10(Kolcifr-1)){
                    case 0: znak = 0; break;
                    case parseInt(max_chislo/10): znak = 1; break;
                    default: znak = randomInteger(2); break;
                }
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                for (let i = 0; i < Kolcifr-per; i++) {
                    let p = possible_checked_var[i][znak][cifr[i]].slice();
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                        p = possible_checked_var[i][znak][cifr[i]].slice();
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < Kolcifr-per; i++) {
                if (cifr[i]>=5 && cifr[i] <= 8) {
                    for (let k = 5; k <= 9; k++){
                        exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                        exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                    }
                }
                else {
                    for (let k = 0; k <= 4; k++){
                        exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                        exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                    }
                }
                if (znak === 0)  cifr[i] += parseInt(chislo_plus[i]);
                else cifr[i] -= parseInt(chislo_plus[i]);
            }

            if (znak === 0) chislo_plus_f();
            else chislo_minus_f();
        }
    }

    /*
     * @private
     */
    function calculate_brother_add_sub() {
        var max_chislo = power10(Kolcifr)-1;
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);

        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;

        if (schotchik === Kolslog) {
            ravno();
        }
        else {
            var vspom_chislo1 = 0, vspom_chislo2 = 0;
            for (let i = 0; i < Kolcifr; i++){
                vspom_chislo1 += 5*power10(i);
                vspom_chislo2 += 7*power10(i);
            }
            if (chislo >= vspom_chislo2) znak = 1;
            else if (chislo < vspom_chislo1) znak = 0;
            else znak = randomInteger(2);
            var razmer = parseInt((chislo.toString()).length);
            let per = 0;
            if (half === true && razmer > 1 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
                switch (chislo%power10(Kolcifr-1)){
                    case 0: znak = 0; break;
                    case parseInt(max_chislo/10): znak = 1; break;
                    default: znak = randomInteger(2); break;
                }
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                for (let i = 0; i < Kolcifr-per; i++) {
                    let p = possible_checked_var[i][znak][cifr[i]].slice();
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                        p = possible_checked_var[i][znak][cifr[i]].slice();
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < Kolcifr-per; i++) {
                if (znak === 0)  cifr[i] += parseInt(chislo_plus[i]);
                else cifr[i] -= parseInt(chislo_plus[i]);
                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }

            if (znak === 0) chislo_plus_f();
            else chislo_minus_f();
        }
    }

    /*
     * @private
     */
    function calculate_friend_add() {
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);
        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;
        if (schotchik === Kolslog) {
            ravno();
        }
        else {
            var razmer = parseInt((chislo.toString()).length);
            let per = 0;
            if (half === true && razmer > 1 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                for (let i = 0; i < Kolcifr-per; i++) {
                    let p = possible_checked_var[i][znak][cifr[i]].slice();
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                        p = possible_checked_var[i][znak][cifr[i]].slice();
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < Kolcifr-per; i++) {
                for (let k = 0; k <= 9; k++){

                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);

                }
            }
            chislo_plus_f();
            cifr.length = 0;
            for (let i = 0; i < Kolcifr; i++) {
                cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
            }

        }
    }

    /*
     * @private
     */
    function calculate_friend_sub() {
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);


        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;
        var razmer = 0;
        razmer = parseInt((chislo.toString()).length);

        if (razmer === 1 || schotchik === Kolslog) {
            ravno();
        }
        else {
            let per = 0;
            if (half === true && razmer > 2  && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = undefined;
                chislo_plus = [];
                for (let i = 0; i < razmer - 1 - per; i++) {
                    let p = possible_checked_var[i][1][cifr[i]];
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][1][cifr[i]] = POSSIBLE_CHECKED_CONST[1][cifr[i]].slice();
                        p = possible_checked_var[i][1][cifr[i]];
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < razmer - 1 - per; i++) {
                for (let k = 0; k <= 9; k++){

                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);

                }
            }

            chislo_minus_f();
            razmer = parseInt((chislo.toString()).length);
            cifr.length = 0;
            for (let i = 0; i < razmer; i++) {
                cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
            }
            cifr.length = razmer - 1;


        }
    }

    /*
     * @private
     */
    function calculate_friend_add_sub() {
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);

        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;

        var razmer = 0;
        razmer = parseInt((chislo.toString()).length);
        if (schotchik === Kolslog) {
            ravno();
        }
        else {
            if (razmer <= Kolcifr) znak = 0;
            else znak =  randomInteger(2);
            let per = 0;
            if (half === true && razmer > 1 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                if (znak === 0){
                    for (let i = 0; i < Kolcifr - per; i++) {
                        let p = possible_checked_var[i][znak][cifr[i]].slice();
                        if (p.length === 0 || p[0] === 0) {
                            possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                            p = possible_checked_var[i][znak][cifr[i]].slice();
                        }
                        chislo_plus[i] = p[randomInteger(p.length)];
                        chislo_plus_all += chislo_plus[i]*power10(i);
                    }
                }
                else{
                    if (razmer > Kolcifr){
                        for (let i = 0; i < Kolcifr - per; i++) {
                            let p = possible_checked_var[i][znak][cifr[i]].slice();
                            if (p.length === 0 || p[0] === 0) {
                                possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                                p = possible_checked_var[i][znak][cifr[i]].slice();
                            }
                            chislo_plus[i] = p[randomInteger(p.length)];
                            chislo_plus_all += chislo_plus[i]*power10(i);
                        }
                    }
                    else{
                        for (let i = 0; i < razmer - 1; i++) {
                            let p = possible_checked_var[i][znak][cifr[i]].slice();
                            if (p.length === 0 || p[0] === 0) {
                                possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                                p = possible_checked_var[i][znak][cifr[i]].slice();
                            }
                            chislo_plus[i] = p[randomInteger(p.length)];
                            chislo_plus_all += chislo_plus[i]*power10(i);
                        }
                    }
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < Kolcifr - per; i++) {

                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }

            }
            if (znak === 0) {
                chislo_plus_f();
                cifr.length = 0;
                for (let i = 0; i < Kolcifr; i++) {
                    cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
                }
            }
            else {
                chislo_minus_f();
                cifr.length = 0;
                for (let i = 0; i < Kolcifr; i++) {
                    cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
                }
            }
        }
    }

    /*
     * @private
     */
    function calculate_combo_add() {
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);

        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;

        if (schotchik === Kolslog) {
            ravno();
        }
        else {
            var razmer = parseInt((chislo.toString()).length);
            let per = 0;
            if (half === true && razmer > 1 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                for (let i = 0; i < Kolcifr-per; i++) {
                    let p = possible_checked_var[i][znak][cifr[i]].slice();
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                        p = possible_checked_var[i][znak][cifr[i]].slice();
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);
            for (let i = 0; i < Kolcifr-per; i++) {
                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }
            chislo_plus_f();
            cifr.length = 0;
            for (let i = 0; i < Kolcifr; i++) {
                cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
            }

        }
    }

    /*
     * @private
     */
    function calculate_combo_sub() {
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation).slice();


        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;
        var razmer = 0;
        razmer = parseInt((chislo.toString()).length);

        if (razmer === 1 || schotchik === Kolslog) {
            ravno();
        }
        else {
            let per = 0;
            if (half === true && razmer > 2 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = undefined;
                chislo_plus = [];


                for (let i = 0; i < razmer - 1 - per; i++) {
                    let p = possible_checked_var[i][1][cifr[i]];
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][1][cifr[i]] = POSSIBLE_CHECKED_CONST[1][cifr[i]].slice();
                        p = possible_checked_var[i][1][cifr[i]];
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < razmer - 1 - per; i++) {
                for (let k = 0; k <= 4; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }

            chislo_minus_f();
            razmer = parseInt((chislo.toString()).length);
            cifr.length = 0;
            for (let i = 0; i < razmer; i++) {
                cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
            }
            cifr.length = razmer - 1;
        }
    }

    /*
     * @private
     */
    function calculate_combo_add_sub() {
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);

        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;

        var razmer = 0;
        razmer = parseInt((chislo.toString()).length);
        if (schotchik === Kolslog) {
            ravno();
        }
        else {
            if (razmer <= Kolcifr) znak = 0;
            else znak =  randomInteger(2);
            let per = 0;
            if (half === true && razmer > 1 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                if (znak === 0){
                    for (let i = 0; i < Kolcifr - per; i++) {
                        let p = possible_checked_var[i][znak][cifr[i]].slice();
                        if (p.length === 0 || p[0] === 0) {
                            possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                            p = possible_checked_var[i][znak][cifr[i]].slice();
                        }
                        chislo_plus[i] = p[randomInteger(p.length)];
                        chislo_plus_all += chislo_plus[i]*power10(i);
                    }
                }
                else{
                    if (razmer > Kolcifr){
                        for (let i = 0; i < Kolcifr - per; i++) {
                            let p = possible_checked_var[i][znak][cifr[i]].slice();
                            if (p.length === 0 || p[0] === 0) {
                                possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                                p = possible_checked_var[i][znak][cifr[i]].slice();
                            }
                            chislo_plus[i] = p[randomInteger(p.length)];
                            chislo_plus_all += chislo_plus[i]*power10(i);
                        }
                    }
                    else{
                        for (let i = 0; i < razmer - 1; i++) {
                            let p = possible_checked_var[i][znak][cifr[i]].slice();
                            if (p.length === 0 || p[0] === 0) {
                                possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                                p = possible_checked_var[i][znak][cifr[i]].slice();
                            }
                            chislo_plus[i] = p[randomInteger(p.length)];
                            chislo_plus_all += chislo_plus[i]*power10(i);
                        }
                    }
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < Kolcifr - per; i++) {
                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }
            if (znak === 0) {
                chislo_plus_f();
                cifr.length = 0;
                for (let i = 0; i < Kolcifr; i++) {
                    cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
                }
            }
            else {
                chislo_minus_f();
                cifr.length = 0;
                for (let i = 0; i < Kolcifr; i++) {
                    cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
                }
            }
        }
    }

    /*
     * @private
     */
    function calculate_random_add() {
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);

        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;

        if (schotchik === Kolslog) {
            ravno();
        }
        else {
            var razmer = parseInt((chislo.toString()).length);
            let per = 0;
            if (half === true && razmer > 1 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                for (let i = 0; i < Kolcifr - per; i++) {
                    let p = possible_checked_var[i][znak][cifr[i]].slice();
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                        p = possible_checked_var[i][znak][cifr[i]].slice();
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < Kolcifr - per; i++) {
                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }
            chislo_plus_f();
            cifr.length = 0;
            for (let i = 0; i < Kolcifr; i++) {
                cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
            }
        }
    }

    /*
     * @private
     */
    function calculate_random_sub() {
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);
        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;
        var razmer = 0;
        razmer = parseInt((chislo.toString()).length);

        if (razmer === 1 || schotchik === Kolslog) {
            ravno();
        }
        else {
            let per = 0;
            if (half === true && razmer > 2 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = undefined;
                chislo_plus = [];


                for (let i = 0; i < razmer - 1 - per; i++) {
                    let p = possible_checked_var[i][1][cifr[i]];
                    if (p.length === 0 || p[0] === 0) {
                        possible_checked_var[i][1][cifr[i]] = POSSIBLE_CHECKED_CONST[1][cifr[i]].slice();
                        p = possible_checked_var[i][1][cifr[i]];
                    }
                    chislo_plus[i] = p[randomInteger(p.length)];
                    chislo_plus_all += chislo_plus[i]*power10(i);
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < razmer - 1 - per; i++) {
                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }
            chislo_minus_f();
            razmer = parseInt((chislo.toString()).length);
            cifr.length = 0;
            for (let i = 0; i < razmer; i++) {
                cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
            }
            cifr.length = razmer - 1;
        }
    }

    /*
     * @private
     */
    function calculate_random_add_sub() {
        var POSSIBLE_CHECKED_CONST =  possible_checked(Level,Operation);
        chislo_plus = []; chislo_plus_all = 0;
        schotchik++;
        var razmer = 0;
        razmer = parseInt((chislo.toString()).length);
        if (schotchik === Kolslog) {
            ravno();
        }
        else {
            if (razmer <= Kolcifr) znak = 0;
            else znak =  randomInteger(2);
            let per = 0;
            if (half === true && razmer > 1 && Kolcifr > 1 && schotchik % 2 === 1){
                per = 1;
            }
            else per = 0;
            do {
                chislo_plus_all = 0;
                chislo_plus = [];
                if (znak === 0){
                    for (let i = 0; i < Kolcifr - per; i++) {
                        let p = possible_checked_var[i][znak][cifr[i]].slice();
                        if (p.length === 0 || p[0] === 0) {
                            possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                            p = possible_checked_var[i][znak][cifr[i]].slice();
                        }
                        chislo_plus[i] = p[randomInteger(p.length)];
                        chislo_plus_all += chislo_plus[i]*power10(i);
                    }
                }
                else{
                    if (razmer > Kolcifr){
                        for (let i = 0; i < Kolcifr - per; i++) {
                            let p = possible_checked_var[i][znak][cifr[i]].slice();
                            if (p.length === 0 || p[0] === 0) {
                                possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                                p = possible_checked_var[i][znak][cifr[i]].slice();
                            }
                            chislo_plus[i] = p[randomInteger(p.length)];
                            chislo_plus_all += chislo_plus[i]*power10(i);
                        }
                    }
                    else{
                        for (let i = 0; i < razmer - 1; i++) {
                            let p = possible_checked_var[i][znak][cifr[i]].slice();
                            if (p.length === 0 || p[0] === 0) {
                                possible_checked_var[i][znak][cifr[i]] = POSSIBLE_CHECKED_CONST[znak][cifr[i]].slice();
                                p = possible_checked_var[i][znak][cifr[i]].slice();
                            }
                            chislo_plus[i] = p[randomInteger(p.length)];
                            chislo_plus_all += chislo_plus[i]*power10(i);
                        }
                    }
                }
            } while (chislo_plus_all === 0);

            for (let i = 0; i < Kolcifr - per; i++) {
                for (let k = 0; k <= 9; k++){
                    exclude(chislo_plus[i],possible_checked_var[i][0][k]);
                    exclude(chislo_plus[i],possible_checked_var[i][1][k]);
                }
            }

            if (znak === 0) {
                chislo_plus_f();
                cifr.length = 0;
                for (let i = 0; i < Kolcifr; i++) {
                    cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
                }
            }
            else {
                cifr.length = 0;
                for (let i = 0; i < Kolcifr; i++) {
                    cifr[i] = parseInt((chislo % power10(i + 1))/power10(i));
                }
                chislo_minus_f();
            }
        }
    }

    return {
        generate,
        generateFractional,
        calculate_simple_add_sub: calculate_simple_add_sub,
        possible_checked: possible_checked
    }
})();

(function (root, factory) {
    let define;
    if (typeof define === "function" && define.amd) {
        define([], factory);
    } else if (typeof exports === "object") {
        module.exports = factory();
    } else {
        root._utils = factory();
    }
}(this, function () {
    return _utils;
}));