import React, { Component, useContext } from 'react';
import './App.css';


import Tutorial from './components/Tutorial'
import TitleScreen from './components/TitleScreen'
import { AbacusProvider, AbacusContext } from "./contexts/AppContext";

import { css } from '@emotion/core';
// First way to import
import { ClipLoader } from 'react-spinners';
// Another way to import


const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

class App extends Component {


    render() {
        return (
            <AbacusProvider location={this.props.location}>
                <Layout />
            </AbacusProvider>
        )
    }
}

export default App



function Layout() {

    let context = useContext(AbacusContext)

    if (context.state.loading) {
        return (
            <div className='sweet-loading'>
                <ClipLoader
                    css={override}
                    sizeUnit={"px"}
                    size={150}
                    color={'#123abc'}
                    loading={context.state.loading}
                />
            </div>
        )
    } else if (context.state.gameStarted === false) {
        return <TitleScreen />
    } else {
        return <Tutorial />
    } 



}


