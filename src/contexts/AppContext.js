import React, { Component } from 'react';
import axios from 'axios';
import queryString from 'query-string';
import { generateEmptyAbacus, calculateValue, resizeAbacus, generateSec } from './functions/cleanfunctions'

export const AbacusContext = React.createContext();

export class AbacusProvider extends Component {
    startResetDrag = false;
    resetX0 = 0;
    resetx1 = 0;
    resetDx = 0;
    state = {
        visibility: true,
        loading: true,
        ansPosition: 0,
        elapsedSec: 0,
        showWinPopup: false,
        selectedOperation: 0,
        selectedSublevel: null,
        gameCompeted: false,
        gameStarted: false,
        seq: [],
        ans: null,
        beadHeight: 0,
        value: 0,
        dragged: {
            c: null,
            r: null
        },
        active: false,
        enableAbacus: true,
        allLevelsState: [],
        abacusState: [],
        serverConfig: {
            config: {
                gamemode: "game"
            },
        },
    }

    saveStatus = (ce, gs) => {
        let params = queryString.parse(this.props.location.search);
        if (Object.entries(params).length === 0) {
            console.log("no token: not saved")
        } else {
            const servers = {
                prod: "https://api.heygo.school",
                dev: "https://ih1608160.vds.myihor.ru"
            }
            let server = servers[params.source] || servers.prod;
            let progress = Math.floor(100 * ce / this.state.tutorialState.sequence.length);
            if (gs === true) {
                progress = 100;
            }
            axios.put(
                `${server}/api/v0.1/courses-management/games?token=${params.token}`,
                {
                    "properties": `{"currentExercise":${ce}, "progress":${progress} }`,
                },
                { headers: { "Content-Type": "application/json" } }
            )
                .then(r => console.log(r.status))
                .catch(e => console.log(e));
        }
    }
    componentDidMount() {
        const servers = {
            prod: "https://api.heygo.school",
            dev: "https://ih1608160.vds.myihor.ru"
        }
        let params = queryString.parse(this.props.location.search);
        let server = servers[params.source] || "https://api.heygo.school"
        let newState = Object.assign({}, this.state);

        if (Object.entries(params).length === 0) {
            newState.loading = false;
            const resp = `
            {
                "config": { 
                  "lesson": 2,
                  "game": [
                    {
                      "mode": "random",
                      "digits": 1,
                      "exercises": 1
                    },
                    {
                      "mode": "random",
                      "digits": 2,
                      "exercises": 1
                    },
                    {
                      "mode": "random",
                      "digits": 3,
                      "exercises": 1
                    }
                  ]
                },
                "properties": {
                  "currentExercise": 0
                }
              }`;
            let config = JSON.parse(resp).config;
            let properties = {};
            properties.currentExercise = 0;
            newState.serverConfig.config = config;
            newState.serverConfig.properties = properties;
            let sec = generateSec(config);
            newState.tutorialState = {
                lesson: config.lesson,
                currentExercise: properties.currentExercise,
                sequence: sec,
            }
            let abacusSize = newState.tutorialState.sequence[newState.tutorialState.currentExercise];
            newState.abacusState = generateEmptyAbacus(String(abacusSize).length);
            this.setState(newState);
        } else {
            axios.get(`${server}/api/v0.1/courses-management/games?token=${params.token}`)
                .then(response => {

                    newState.loading = false;
                    let config = JSON.parse(response.data.data.config)
                    let properties = JSON.parse(response.data.data.properties);
                    if (Object.entries(properties).length === 0 && properties.constructor === Object) {
                        properties.currentExercise = 0;
                        properties.progress = 0;
                    }

                    newState.serverConfig.config = config;
                    newState.serverConfig.properties = properties;

                    let sec = generateSec(config);

                    if (properties.currentExercise === sec.length) {
                        properties.currentExercise = 0;
                        properties.progress = 0;
                    }
                    newState.tutorialState = {
                        lesson: config.lesson,
                        currentExercise: properties.currentExercise,
                        sequence: sec,
                    }

                    if (config.lesson === 2) {
                        let abacusSize = newState.tutorialState.sequence[newState.tutorialState.currentExercise];
                        newState.abacusState = generateEmptyAbacus(String(abacusSize).length);
                    } else if (config.lesson === 4) {
                        let Kolcifr = newState.tutorialState.sequence[newState.tutorialState.currentExercise].Kolcifr;
                        newState.abacusState = generateEmptyAbacus(Kolcifr);

                    } else if (config.lesson >= 7) {
                        let Kolcifr = newState.tutorialState.sequence[newState.tutorialState.currentExercise].Kolcifr;
                        newState.abacusState = generateEmptyAbacus(Kolcifr + 1);
                    } else {
                        let Kolcifr = newState.tutorialState.sequence[newState.tutorialState.currentExercise].Kolcifr;
                        newState.abacusState = generateEmptyAbacus(Kolcifr);
                    }
                    this.setState(newState);

                });
        }
    }

    delayState = () => {
        console.log("starttimer");
        this.mydelay = setTimeout(() => {
            let newState = Object.assign({}, this.state);
            if (newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer !== newState.value) {
                newState.visibility = false;
            }

            this.setState(newState);
        }, 4000);
    }


    setVisible = (e) => {
        console.log("stop!");
        clearInterval(this.mydelay)
        let newState = Object.assign({}, this.state);
        newState.visibility = true;
        this.setState(newState);
    }
    dragStart = (e) => {

        if (this.state.enableAbacus) {
            try {

                if (e.target.id === "sp0" || e.target.id === "circle") {
                    this.startResetDrag = true;
                    if (e.type === "touchstart") {
                        this.resetX0 = e.touches[0].clientX
                    } else {
                        this.resetX0 = e.clientX
                    }
                }

                let newState = Object.assign({}, this.state);
                if (newState.tutorialState.lesson === 4) {
                    this.setVisible();
                }
                newState.visibility = true;
                newState.beadHeight5 = document.querySelector("#uc").getBoundingClientRect().height - 8;
                newState.beadHeight1 = (document.querySelector("#bc").getBoundingClientRect().height - 8) / 2.5;
                newState.dragged.c = parseInt(e.target.id[0]);
                newState.dragged.r = parseInt(e.target.id[2]);
                newState.levelState = "started";
                let c = parseInt(e.target.id[0]);
                let r = parseInt(e.target.id[2]);



                if (e.type === "touchstart") {
                    newState.abacusState[c][r].initialY = e.touches[0].clientY - this.state.abacusState[c][r].yOffset;
                } else {
                    newState.abacusState[c][r].initialY = e.clientY - this.state.abacusState[c][r].yOffset;
                }

                if (e.target !== e.currentTarget) {
                    // newState.abacusState[c][r].active = true;
                    newState.active = true;
                }

                this.setState(newState);
            } catch (e) {

            }
        }

    }

    drag = (e) => {
        if (this.state.enableAbacus) {
            let c, r;
            try {

                //   if (this.state.abacusState[c][r].active === true) {
                if (this.state.active === true) {

                    c = this.state.dragged.c;
                    r = this.state.dragged.r;
                    let newState = Object.assign({}, this.state);
                    let uch5 = newState.beadHeight5;
                    let uch1 = newState.beadHeight1;
                    newState.visibility = true;
                    let currentY;
                    if (e.type === "touchmove") {
                        currentY = e.touches[0].clientY - this.state.abacusState[c][r].initialY;
                    } else {
                        currentY = e.clientY - this.state.abacusState[c][r].initialY;
                    }

                    if (!this.state.abacusState[c][r].beadState) {
                        if (r === 0) {
                            if (currentY >= 0) {
                                if (currentY < uch5 / 4) {
                                    newState.abacusState[c][r].currentY = currentY
                                    //this.setTranslate(newState.abacusState[c][r].currentY, dragItem);
                                } else {
                                    currentY = uch5 / 2
                                    //this.setTranslate(newState.abacusState[c][r].currentY, dragItem);

                                    newState.abacusState[c][r].yOffset = currentY;
                                    newState.abacusState[c][r].currentY = currentY;
                                    newState.abacusState[c][r].beadState = 1;
                                    // newState.abacusState[c][r].active = false;
                                    newState.active = false;
                                    this.setState(newState);
                                    return false;
                                }
                            }
                        }

                        if ([1, 2, 3, 4].includes(r)) {
                            if (currentY < 0) {
                                if (currentY > -1 * uch1 / 4) {
                                    // this.setTranslate(currentY, dragItem);
                                    newState.abacusState[c][r].currentY = currentY;
                                    let firstOne = 1;
                                    for (let ind = 1; ind <= 4; ind++) {
                                        if (newState.abacusState[c][ind].beadState === 1) {
                                            firstOne = ind + 1;
                                        }
                                    }

                                    for (let ind = firstOne; ind <= r; ind++) {
                                        newState.abacusState[c][ind].currentY = currentY;
                                        newState.abacusState[c][ind].yOffset = currentY;
                                    }
                                } else {
                                    currentY = -1 * uch1 / 2;

                                    for (let ind = 1; ind <= r; ind++) {
                                        // let item = document.querySelector("#bead_" + c + "_" + ind);
                                        //this.setTranslate(newState.abacusState[c][r].currentY, item);

                                        newState.abacusState[c][ind].beadState = 1;
                                        newState.abacusState[c][ind].yOffset = currentY;
                                        newState.abacusState[c][ind].currentY = currentY;
                                    }

                                    // newState.abacusState[c][r].active = false;
                                    newState.active = false;
                                    this.setState(newState);
                                    return false;
                                }
                            }
                        }
                    }

                    if (newState.abacusState[c][r].beadState) {

                        if (r === 0) {
                            if (newState.abacusState[c][r].currentY > uch5 / 4 && newState.abacusState[c][r].currentY <= uch5 / 2) {

                                // this.setTranslate(newState.abacusState[c][r].currentY, dragItem);
                                newState.abacusState[c][r].currentY = currentY;
                                newState.abacusState[c][r].yOffset = currentY;
                            }
                            else if (currentY <= uch5 / 4) {
                                currentY = 0;
                                // this.setTranslate(newState.abacusState[c][r].currentY, dragItem);
                                newState.abacusState[c][r].currentY = currentY;
                                newState.abacusState[c][r].yOffset = currentY;
                                newState.abacusState[c][r].beadState = 0;
                                // newState.abacusState[c][r].active = false;
                                newState.active = false;
                                this.setState(newState);
                                return false;
                            }
                        }

                        if ([1, 2, 3, 4].includes(r)) {
                            if (currentY < 0) {
                                if (currentY <= -1 * uch1 / 4 && currentY > -1 * uch1 / 2) {
                                    let firstZero = 4;
                                    for (let ind = 4; ind > 0; ind--) {
                                        if (newState.abacusState[c][ind].beadState === 0) {
                                            firstZero = ind - 1;
                                        }
                                    }
                                    for (let ind = r; ind <= firstZero; ind++) {
                                        newState.abacusState[c][ind].currentY = currentY;
                                        newState.abacusState[c][ind].yOffset = currentY;
                                    }
                                }
                                if (currentY > -1 * uch1 / 4) {
                                    currentY = 0;
                                    //this.setTranslate(currentY, dragItem);
                                    for (let ind = r; ind <= 4; ind++) {
                                        newState.abacusState[c][ind].beadState = 0;
                                        newState.abacusState[c][ind].yOffset = currentY;
                                        newState.abacusState[c][ind].currentY = currentY;
                                    }
                                    // newState.abacusState[c][r].active = false;
                                    newState.active = false;
                                    this.setState(newState);
                                    return false;
                                }
                            }
                        }
                    }
                    this.setState(newState);
                }
            } catch (e) {
            }
        }
    }

    dragEnd = (e) => {
        if (this.state.enableAbacus) {
            if (this.startResetDrag) {
                this.startResetDrag = false;
                if (e.type === "touchstart") {
                    this.resetX1 = e.touches[0].clientX
                } else {
                    this.resetX1 = e.clientX
                }
                if (this.resetX1 < this.resetX0) {
                    this.resetAbacus();
                }
            }

            try {
                let c = this.state.dragged.c;
                let r = this.state.dragged.r;
                let newState = Object.assign({}, this.state);
                let uch5 = newState.beadHeight5;
                let uch1 = newState.beadHeight1;
                let currentY = newState.abacusState[c][r].currentY;

                if (!newState.abacusState[c][r].beadState) {

                    if (r === 0) {
                        if (currentY < uch5 / 2) {
                            currentY = 0;
                            newState.abacusState[c][r].yOffset = currentY;
                            newState.abacusState[c][r].currentY = currentY;
                        }
                    }
                    if ([1, 2, 3, 4].includes(r)) {
                        if (currentY > -1 * uch1 / 4) {
                            let firstOne = 1;
                            for (let ind = 1; ind <= 4; ind++) {
                                if (newState.abacusState[c][ind].beadState === 1) {
                                    firstOne = ind + 1;
                                }
                            }
                            for (let ind = firstOne; ind <= r; ind++) {
                                newState.abacusState[c][ind].currentY = 0;
                                newState.abacusState[c][ind].yOffset = 0;
                            }
                        }
                    }
                }

                if (newState.abacusState[c][r].beadState) {
                    if (r === 0) {
                        if (currentY > uch5 / 4) {
                            currentY = uch5 / 2;
                            newState.abacusState[c][r].currentY = currentY;
                            newState.abacusState[c][r].yOffset = currentY;
                        }
                    }
                    if ([1, 2, 3, 4].includes(r)) {

                        if (newState.abacusState[c][r].currentY > -1 * uch1 / 2) {
                            let firstZero = 4;

                            for (let ind = 4; ind > 0; ind--) {
                                if (newState.abacusState[c][ind].beadState === 0) {
                                    firstZero = ind - 1;
                                }
                            }
                            for (let ind = r; ind <= firstZero; ind++) {
                                currentY = -1 * uch1 / 2;
                                newState.abacusState[c][ind].yOffset = currentY;
                                newState.abacusState[c][ind].currentY = currentY;
                            }
                        }
                    }
                }
                newState.abacusState[c][r].initialY = currentY;
                newState.active = false;
                newState.dragged.c = null;
                newState.dragged.r = null;
                newState.value = calculateValue(newState.abacusState);
                if (newState.tutorialState.lesson === 2) {
                    let abacusSize;
                    if (newState.value === newState.tutorialState.sequence[newState.tutorialState.currentExercise]) {
                        if (newState.tutorialState.currentExercise < newState.tutorialState.sequence.length - 1) {
                            newState.tutorialState.currentExercise += 1;
                            abacusSize = newState.tutorialState.sequence[newState.tutorialState.currentExercise];
                            newState.abacusState = resizeAbacus(abacusSize, newState.abacusState);
                        } else {
                            newState.enableAbacus = false;
                            newState.gameCompeted = true;
                            newState.tutorialState.currentExercise = 0;
                        }
                        this.saveStatus(newState.tutorialState.currentExercise, newState.gameCompeted);
                    }
                } else if (newState.tutorialState.lesson === 4) {
                    if (newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer === newState.value) {
                        if (newState.tutorialState.currentExercise === newState.tutorialState.sequence.length - 1) {
                            newState.gameCompeted = true;
                            newState.tutorialState.currentExercise = 0;
                            clearInterval(this.mydelay);
                            this.saveStatus(newState.tutorialState.currentExercise, newState.gameCompeted);
                        }
                    }
                } else {




                    if (newState.tutorialState.currentExercise <= newState.tutorialState.sequence.length - 1) {
                        let progressSum = 0;
                        for (let i = 0; i < newState.ansPosition + 1; i++) {
                            progressSum += newState.tutorialState.sequence[newState.tutorialState.currentExercise].seq[i];
                        }
                        if (newState.value === progressSum) {
                            newState.ansPosition += 1;
                        }

                        if (newState.tutorialState.currentExercise === newState.tutorialState.sequence.length - 1) {
                            if (newState.tutorialState.sequence[newState.tutorialState.currentExercise].seq.length === newState.ansPosition &&
                                newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer === newState.value) {
                                newState.gameCompeted = true;
                                newState.tutorialState.currentExercise = 0;
                                this.saveStatus(newState.tutorialState.currentExercise, newState.gameCompeted);
                            }
                        }
                    }
                }
                this.setState(newState);
            } catch (e) {

            }
        }
        if (this.state.tutorialState.lesson === 4) {
            this.delayState();
        }
    }

    resetAbacus = () => {
        let newState = Object.assign({}, this.state);

        if (newState.tutorialState.lesson === 2) {
            newState.abacusState = generateEmptyAbacus(String(newState.tutorialState.sequence[newState.tutorialState.currentExercise]).length);
            //this.saveStatus();
        } else if (newState.tutorialState.lesson === 4) {
            if (newState.value === newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer) {
                newState.tutorialState.currentExercise += 1;
                this.saveStatus(newState.tutorialState.currentExercise, newState.gameCompeted);
                this.delayState();
            }
            let Kolcifr = newState.tutorialState.sequence[newState.tutorialState.currentExercise].Kolcifr;
            newState.abacusState = generateEmptyAbacus(Kolcifr);
        } else {
            if (newState.value === newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer) {
                newState.tutorialState.currentExercise += 1;
                this.saveStatus(newState.tutorialState.currentExercise, newState.gameCompeted);
            }
            let Kolcifr = newState.tutorialState.sequence[newState.tutorialState.currentExercise].Kolcifr;
            if (newState.tutorialState.lesson >= 7) {
                Kolcifr = Kolcifr + 1;
            }
            newState.abacusState = generateEmptyAbacus(Kolcifr);
        }
        newState.ansPosition = 0;
        newState.value = 0;

        this.setState(newState);
    }


    resetStart = () => {
        document.getElementById('reset').style.backgroundPositionY = '20px';
    }

    resetEnd = () => {
        document.getElementById('reset').style.backgroundPositionY = '10px';
        this.resetAbacus();
    }

    restartTutorial = () => {
        let newState = Object.assign({}, this.state);
        newState.value = 0;
        newState.ansPosition = 0;
        newState.enableAbacus = true;
        newState.visibility = true;
        clearInterval(this.mydelay);
        let sec = generateSec(newState.serverConfig.config);
        newState.tutorialState.sequence = sec;
        newState.tutorialState.currentExercise = 0;

        if (newState.serverConfig.config.lesson === 2) {
            let abacusSize = newState.tutorialState.sequence[newState.tutorialState.currentExercise];
            newState.abacusState = generateEmptyAbacus(String(abacusSize).length);
        } else {
            if (newState.serverConfig.config.lesson === 4) {
                this.delayState();
            }
            let Kolcifr = newState.tutorialState.sequence[newState.tutorialState.currentExercise].Kolcifr;
            if (newState.tutorialState.lesson >= 7) {
                Kolcifr = Kolcifr + 1;
            }
            newState.abacusState = generateEmptyAbacus(Kolcifr);
        }
        newState.gameCompeted = false;

        this.setState(newState);
        this.saveStatus(newState.tutorialState.currentExercise, newState.gameCompeted);


    }
    startTutorial = () => {
        let newState = Object.assign({}, this.state);
        newState.gameStarted = true;
        if (newState.tutorialState.lesson === 4) {
            this.delayState();
        }
        this.setState(newState);
    }


    startTimer = () => {
        this.timer = setInterval(() => {
            let newState = Object.assign({}, this.state);
            newState.allLevelsState[newState.selectedOperation].sublevels[newState.selectedSublevel].elapsedTime += 1;
            this.setState(newState);
        }, 1000);
    }


    stopTimer = () => {
        clearInterval(this.timer)
    }

    resetTimer = () => {
        let newState = Object.assign({}, this.state);
        newState.allLevelsState[newState.selectedOperation].sublevels[newState.selectedSublevel].elapsedTime = 0;
        this.setState(newState);
    }

    selectOperation = (index) => {
        let newState = Object.assign({}, this.state);
        newState.selectedOperation = index;
        this.setState(newState);
    }
    nextLevel = () => {
        let newState = Object.assign({}, this.state);
        newState.ansPosition = 0;
        newState.levelState = "notentered";
        newState.value = 0;
        newState.abacusState = generateEmptyAbacus();

        let res = process();
        newState.seq = res.seq;
        newState.ans = res.answer[0];
        this.setState(newState);
    }

    componentDidUpdate() {
        let intermedateSum = 0;
        for (let i = 0; i <= this.state.ansPosition; i++) {
            intermedateSum += this.state.seq[i];
        }
        if (this.state.value === intermedateSum) {
            let newState = Object.assign({}, this.state);
            newState.ansPosition = this.state.ansPosition + 1;
            if (newState.ansPosition === 3) {
                newState.levelState = "finished";
                //this.nextLevel();
            }
            this.setState(newState);
        }
    }

    render() {
        return (
            <AbacusContext.Provider
                value={{
                    state: this.state,
                    actions: {
                        selectOperation: this.selectOperation,
                        nextExample: this.nextExample,
                        restartTutorial: this.restartTutorial,
                        startTutorial: this.startTutorial,
                        nextLevel: this.nextLevel,
                        resetStart: this.resetStart,
                        resetEnd: this.resetEnd,
                        dragStart: this.dragStart,
                        drag: this.drag,
                        dragEnd: this.dragEnd,
                        startTimer: this.startTimer,
                        stopTimer: this.stopTimer,
                    },
                }}
            >
                {this.props.children}
            </AbacusContext.Provider>
        );
    }
}

export const AbacusConsumer = AbacusContext.Consumer 