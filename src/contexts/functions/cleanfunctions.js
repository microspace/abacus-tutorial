import utils from '../../libs/generator'

function generateEx(cfg, mode, vmode) {
    let flag = 0;
    let res, ans;
    while (flag === 0) {
        res = utils.generate(cfg);
        ans = res.answer.reverse().reduce(function (a, b) { return a * 10 + b });
        ans === 0 ? flag = 0 : flag = 1;
    }
    return { seq: res.seq, answer: ans, Kolcifr: cfg.Kolcifr, mode: mode, vmode: vmode }
}

const generateEmptyAbacus = (ans) => {
    let c = ans;
    let r = 5;
    let abacus = [];
    for (let j = 1; j <= c; j++) {
        abacus.push([])
        for (let i = 1; i <= r; i++) {
            abacus[j - 1].push({
                beadState: 0,
                active: false,
                currentY: 0,
                initialY: 0,
                yOffset: 0,
            })
        }
    }
    return abacus
}

const resizeAbacus = (ans, oldAbacus) => {
    //document.getElementById("reset").setAttribute("class", "reset");
    let c = String(ans).length;
    let r = 5;
    let emptycolumn = [];

    for (let i = 1; i <= r; i++) {
        emptycolumn.push({
            beadState: 0,
            active: false,
            currentY: 0,
            initialY: 0,
            yOffset: 0,
        })

    }
    const addedrowcount = c - oldAbacus.length;
    for (let i = 1; i <= addedrowcount; i++) {
        oldAbacus.push(emptycolumn);
    }
    return oldAbacus
}

function calculateValue(abacusState) {

    let sum = 0;
    for (let i = 0; i < abacusState.length; i++) {
        sum += (abacusState[i][0].beadState * 5 + Math.max(abacusState[i][1].beadState, abacusState[i][2]
            .beadState * 2, abacusState[i][3].beadState * 3, abacusState[i][4].beadState * 4)) * (10 ** i);
    }
    return sum;
}


const generateSec = (config) => {

    let arr1 = [];

    if (config.lesson === 2) {

        config.game.forEach(element => {
            if (element.mode === "random") {
                let ex = element.exercises;
                if (element.digits === "1-2") {
                    element.digits = 2
                }
                arr1 = arr1.concat(getRandomInts(ex, 10 ** (element.digits - 1) - 1, 10 ** element.digits));
            }
        });
        return arr1;

    } else if (config.lesson === 3) {

        config.game.forEach(element => {
            if (element.mode === "simple") {
                if (element.digits === "1-2") {
                    element.digits = 2
                }
                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                        Level: 1,
                        Operation: 2,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))
                }


            }
        });
        return arr1
    } else if (config.lesson === 4) {

        config.game.forEach(element => {
            if (element.mode === "simpleviz") {
                if (element.digits === "1-2") {
                    element.digits = 2
                }
                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                        Level: 1,
                        Operation: 2,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))
                }


            }
        });
        return arr1
    } else if (config.lesson === 5) {

        config.game.forEach(element => {
            if (element.digits === "1-2") {
                element.digits = 2
            }
            if (element.mode === "brother+1") {

                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [1],
                        Level: 2,
                        Operation: 0,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))

                }


            } else if (element.mode === "brother+2") {

                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [2],
                        Level: 2,
                        Operation: 0,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))

                }


            } else if (element.mode === "brother+3") {

                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [3],
                        Level: 2,
                        Operation: 0,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))

                }


            } else if (element.mode === "brother+4") {

                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [4],
                        Level: 2,
                        Operation: 0,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))

                }


            } else if (element.mode === "brother+1234") {

                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [1, 2, 3, 4],
                        Level: 2,
                        Operation: 0,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))

                }


            }
        });
        return arr1
    } else if (config.lesson === 6) {

        config.game.forEach(element => {
            if (element.digits === "1-2") {
                element.digits = 2
            }
            if (element.mode === "brother-1") {

                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [1],
                        Level: 2,
                        Operation: 1,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))

                }


            } else if (element.mode === "brother-2") {

                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [2],
                        Level: 2,
                        Operation: 1,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))

                }


            } else if (element.mode === "brother-3") {

                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [3],
                        Level: 2,
                        Operation: 1,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))

                }


            } else if (element.mode === "brother-4") {

                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [4],
                        Level: 2,
                        Operation: 1,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))

                }


            } else if (element.mode === "brother-1234") {

                for (let i = 0; i < element.exercises; i++) {
                    const cfg = {
                        Checked_cifr: [1, 2, 3, 4],
                        Level: 2,
                        Operation: 1,
                        Kolslog: element.addends,
                        Kolcifr: element.digits
                    }
                    arr1.push(generateEx(cfg, element.mode, element.mode))

                }


            }
        });
        return arr1
    } else if (config.lesson === 7) {
        config.game.forEach(element => {
            if (element.digits === "1-2") {
                element.digits = 2
            }
            for (let i = 0; i < element.exercises; i++) {
                let cf = element.operands.split(",");
               
                var result = cf.map(function (x) {
                    return parseInt(x, 10);
                });

                
                const cfg = {
                    Checked_cifr: result,
                    Level: 3,
                    Operation: 0,
                    Kolslog: element.addends,
                    Kolcifr: element.digits
                }
                //arr1.push(generateEx(cfg, "друг +" + element.operands))
                arr1.push(generateEx(cfg, "Помощь друга +", "friend+" + element.operands))
            }

        });
        return arr1
    } else if (config.lesson === 8) {

        config.game.forEach(element => {
            if (element.digits === "1-2") {
                element.digits = 2
            }
            for (let i = 0; i < element.exercises; i++) {

                let cf = element.operands.split(",");
               
                var result = cf.map(function (x) {
                    return parseInt(x, 10);
                });

                
                const cfg = {
                    Checked_cifr: result,
                    Level: 3,
                    Operation: 1,
                    Kolslog: element.addends,
                    Kolcifr: element.digits
                }
              //  arr1.push(generateEx(cfg, "друг -" + element.operands))
                arr1.push(generateEx(cfg, "Помощь друга -", "friend-" + element.operands))
                
            }

        });
        return arr1
    } else if (config.lesson === 9) {

        config.game.forEach(element => {
            if (element.digits === "1-2") {
                element.digits = 2
            }
            for (let i = 0; i < element.exercises; i++) {

                let cf = element.operands.split(",");
               
                var result = cf.map(function (x) {
                    return parseInt(x, 10);
                });

                
                const cfg = {
                    Checked_cifr: result,
                    Level: 4,
                    Operation: 0,
                    Kolslog: element.addends,
                    Kolcifr: element.digits
                } 
                //arr1.push(generateEx(cfg, "Комбо +" + element.operands))
                arr1.push(generateEx(cfg, "Комбинированный", "combo+" + element.operands))
            }

        });
        return arr1
    } else if (config.lesson === 10) {

        config.game.forEach(element => {
            if (element.digits === "1-2") {
                element.digits = 2
            }
            for (let i = 0; i < element.exercises; i++) {
          
                let cf = element.operands.split(",");
               
                var result = cf.map(function (x) {
                    return parseInt(x, 10);
                });

                
                const cfg = {
                    Checked_cifr: result,
                    Level: 4,
                    Operation: 1,
                    Kolslog: element.addends,
                    Kolcifr: element.digits
                }
                //arr1.push(generateEx(cfg, "Комбо -" + element.operands))
                arr1.push(generateEx(cfg, "Комбинированный", "combo-" + element.operands))
            }

        });
        return arr1
    }  else if (config.lesson === 11) {

        config.game.forEach(element => {
            if (element.digits === "1-2") {
                element.digits = 2
            }
            for (let i = 0; i < element.exercises; i++) {
       
                let cf = element.operands.split(",");
               
                var result = cf.map(function (x) {
                    return parseInt(x, 10);
                });

                
                const cfg = {
                    Checked_cifr: result,
                    Level: 5,
                    Operation: 2,
                    Kolslog: element.addends,
                    Kolcifr: 2
                }
                arr1.push(generateEx(cfg, "Произвольный", "random"))
            }

        });
        return arr1
    }
}





function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomInts(num, min, max) {
    var ints = [];
    while (ints.length < num) {
        var randNum = getRandomInt(min, max);
        if (ints.indexOf(randNum) === -1) {
            ints.push(randNum);
        }
    }

    while (ints[0] === 0) {
        shuffleArray(ints)
    }
    return ints;
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}



export {
    generateEmptyAbacus,
    calculateValue,
    resizeAbacus,
    generateSec,
    generateEx
}

/* var level = { level: 2, Checked_cifr: [1] };
+
+        var cfg = {
+            Checked_cifr: level.Checked_cifr,
+            Level: level.level,
+            Operation: 2,
+            Kolslog: 3,
+            Kolcifr: 1
+        } */