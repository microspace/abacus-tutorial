import React from 'react';
import './Status.css'

class Status extends React.Component {

  render() {
    let current = this.props.tutorialState.currentExercise + 1;
    let total = this.props.tutorialState.sequence.length;
/*     if (this.props.tutorialState.currentExercise === this.props.tutorialState.sequence.length) {
      stats = null;
    } */
    return (
      <>{current}<span className="totalcolor">&nbsp;/&nbsp;{total}</span></>
    )
  }
}


export default Status
