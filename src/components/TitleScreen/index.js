import React from "react";
import { AbacusContext } from "../../contexts/AppContext";
import TitleScreen from "./TitleScreen";

export default props => (
    <AbacusContext.Consumer>
        {
            ({ state: {tutorialState}, actions: {startTutorial, startGame} }) => <TitleScreen 
        {...props} 
        tutorialState={tutorialState}
        startTutorial={startTutorial}
        startGame={startGame}

        />}
    </AbacusContext.Consumer>
);

