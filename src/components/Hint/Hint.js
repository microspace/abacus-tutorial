import React from 'react';
import './Hint.css'


const hints = {
  "simple": "Сложение/вычитание",
  "simpleviz": "Счёт в уме",
  "brother+1": "Помощь брата +",
  "brother+2": "Помощь брата +",
  "brother+3": "Помощь брата +",
  "brother+4": "Помощь брата +",
  "brother+1234": "Помощь брата +",
  "brother-1": "Помощь брата -",
  "brother-2": "Помощь брата -",
  "brother-3": "Помощь брата -",
  "brother-4": "Помощь брата -",
  "brother-1234": "Помощь брата -",

}
class Hint extends React.Component {

  render() {
    let stattext;
    if (this.props.tutorialState.lesson === 2) {
      stattext = "Счёт"
    } else if (this.props.tutorialState.lesson > 2 && this.props.tutorialState.lesson < 7) {
      stattext = hints[this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].mode];
    } else if (this.props.tutorialState.lesson >= 7) {
      stattext = this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].mode;
    } 
    return (
    
        <div className="hints">{stattext}</div>
    )
  }
}


export default Hint
