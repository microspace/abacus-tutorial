import React from "react";
import { AbacusContext } from "../../contexts/AppContext";
import Status from "./Hint";

export default props => (
    <AbacusContext.Consumer>
        {
            ({ state: {tutorialState}, actions: {restartTutorial} }) => <Status 
        {...props} 
        tutorialState={tutorialState}
        restartTutorial={restartTutorial}
        />}
    </AbacusContext.Consumer>
);

