import React, { Component } from 'react';
import './Exercise.css';


class Exercise extends Component {

    render() {


        if (this.props.tutorialState.lesson === 2) {
            return (
                <div className="numbercont">
                    <div className="tt tt_min">{this.props.tutorialState.sequence[this.props.tutorialState.currentExercise]}</div>
                    <hr className="hline" />
                </div>
            )
        } else {
            const seqlth = this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].seq.length;
            let tsize;

            if (seqlth <= 4) {
                tsize = "tt tt_mid"
            } else {
                tsize = "tt tt_max"
            }
            if (this.props.tutorialState.lesson === 4) {


                return (
                    <div className="numbercont">
                        {this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].seq.map((currentValue, index, arr) => (
                            <div className={`${tsize}`} key={index}>{this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].seq[index]}</div>
                        ))}
                        <hr className="hline" />
                        <div className={`${tsize}`}>{this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].answer === this.props.value ? this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].answer : "\u00a0"}</div>
                    </div>
                )
            } else {
                return (
                    <div className="numbercont">
                        {this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].seq.map((currentValue, index, arr) => (
                            <div className={`${tsize}`} key={index} style={this.props.ansPosition === index ? { color: "red" } : { color: "black" }}>{this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].seq[index]}</div>
                        ))}
                        <hr className="hline" />
                        <div className={`${tsize}`} >{this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].seq.length === this.props.ansPosition ? this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].answer : "\u00a0"}</div>
                    </div>
                )
            }
        }

    }

}




export default Exercise