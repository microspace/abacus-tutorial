import React from "react";
import { AbacusContext } from "../../contexts/AppContext";
import Exercise from "./Exercise";

export default props => (
    <AbacusContext.Consumer>
        {
            ({ state: {tutorialState, ansPosition, value}}) => <Exercise 
        {...props} 
        tutorialState={tutorialState}
        ansPosition={ansPosition}
        value={value}

        />}
    </AbacusContext.Consumer>
);

