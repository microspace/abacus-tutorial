import React from 'react';
import './Helper.css'



const helpertext = {
  "brother+1": ["+1 = +5 -4"],
  "brother+2": ["+2 = +5 -3"],
  "brother+3": ["+3 = +5 -2"],
  "brother+4": ["+4 = +5 -1"],
  "brother+1234": [
    "+1 = +5 -4",
    "+2 = +5 -3",
    "+3 = +5 -2",
    "+4 = +5 -1"],
  "brother-1": ["-1 = -5 +4"],
  "brother-2": ["-2 = -5 +3"],
  "brother-3": ["-3 = -5 +2"],
  "brother-4": ["-4 = -5 +1"],
  "brother-1234": [
    "-1 = -5 +4",
    "-2 = -5 +3",
    "-3 = -5 +2",
    "-4 = -5 +1"],
  "friend+1": ["+1 = +10 -9"],
  "friend+2": ["+2 = +10 -8"],
  "friend+3": ["+3 = +10 -7"],
  "friend+4": ["+4 = +10 -6"],
  "friend+5": ["+5 = +10 -5"],
  "friend+6": ["+6 = +10 -4"],
  "friend+7": ["+7 = +10 -3"],
  "friend+8": ["+8 = +10 -2"],
  "friend+9": ["+9 = +10 -1"],
  "friend+1,2,3,4,5,6,7,8,9": [
    "+1 = +10 -9",
    "+2 = +10 -8",
    "+3 = +10 -7",
    "+4 = +10 -6",
    "+5 = +10 -5",
    "+6 = +10 -4",
    "+7 = +10 -3",
    "+8 = +10 -2",
    "+9 = +10 -1",
  ],
  "friend-1": ["-1 = -10 +9"],
  "friend-2": ["-2 = -10 +8"],
  "friend-3": ["-3 = -10 +7"],
  "friend-4": ["-4 = -10 +6"],
  "friend-5": ["-5 = -10 +5"],
  "friend-6": ["-6 = -10 +4"],
  "friend-7": ["-7 = -10 +3"],
  "friend-8": ["-8 = -10 +2"],
  "friend-9": ["-9 = -10 +1"],
  "friend-1,2,3,4,5,6,7,8,9": [
    "-1 = -10 +9",
    "-2 = -10 +8",
    "-3 = -10 +7",
    "-4 = -10 +6",
    "-5 = -10 +5",
    "-6 = -10 +4",
    "-7 = -10 +3",
    "-8 = -10 +2",
    "-9 = -10 +1",
  ],
  "combo+6": ["+6 = +10 -5 +1"],
  "combo+7": ["+7 = +10 -5 +2"],
  "combo+8": ["+8 = +10 -5 +3"],
  "combo+9": ["+9 = +10 -5 +4"],
  "combo+6,7,8,9": [
    "+6 = +10 -5 +1",
    "+7 = +10 -5 +2",
    "+8 = +10 -5 +3",
    "+9 = +10 -5 +4",
  ],
  "combo-6": ["-6 = -10 +5 -1"],
  "combo-7": ["-7 = -10 +5 -2"],
  "combo-8": ["-8 = -10 +5 -3"],
  "combo-9": ["-9 = -10 +5 -4"],
  "combo-6,7,8,9": [
    "-6 = -10 +5 -1",
    "-7 = -10 +5 -2",
    "-8 = -10 +5 -3",
    "-9 = -10 +5 -4",
  ],
}



class Helper extends React.Component {

  render() {
    if (this.props.lesson === 2 || this.props.lesson === 3 || this.props.lesson === 4 || this.props.lesson === 11) {
      return (
        <div className="lesson2"></div>
      )
    } else {
      return (
        <div className="roundedbox">
          {helpertext[this.props.mode].map((cv, index, arr) => (
            <div className="listitem" key={index}>{helpertext[this.props.mode][index]}</div>
          ))}
        </div>
      )
    }
  }
}


export default Helper
