import React from "react";
import { AbacusContext } from "../../contexts/AppContext";
import Timer from "./Timer";

export default props => (
    <AbacusContext.Consumer>
        {
            ({ state: {selectedOperation, selectedSublevel, allLevelsState}, actions: {startTimer, stopTimer} }) => <Timer 
        {...props} 
        selectedOperation={selectedOperation}
        selectedSublevel={selectedSublevel}
        allLevelsState={allLevelsState}
        startTimer={startTimer} 
        stopTimer={stopTimer} 
        />}
    </AbacusContext.Consumer>
);

