import React from "react";
import { AbacusContext } from "../../contexts/AppContext";
import Tutorial from "./Tutorial";

export default props => (
    <AbacusContext.Consumer>
        {
            ({ state: {tutorialState, visibility, serverConfig, gameCompeted}, actions: {restartTutorial} }) => <Tutorial 
        {...props} 

        tutorialState={tutorialState}
        restartTutorial={restartTutorial} 
        visibility={visibility} 
        serverConfig={serverConfig} 
        gameCompeted={gameCompeted}
        />}
    </AbacusContext.Consumer>
);

