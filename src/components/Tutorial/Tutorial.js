import React from 'react';

import Abacus from '../Abacus'
import Exercise from '../Exercise'
import Status from '../Status'
import Hint from '../Hint'
import Helper from '../Helper/Helper'
import "./Tutorial.css"
import mouse from './mouse.svg'


class Tutorial extends React.Component {

  componentDidMount() {
    document.body.style = 'background: #EDC0D7;';
  }

  render() {



    return (
      <>
        <div className="overlay" style={this.props.gameCompeted === false ? { display: 'none' } : { display: 'block' }}>
          <div className="popupbox" style={{backgroundImage: "url(" + mouse + ")"}}>
            <div className="titlep">Молодец!</div>
            <div className="textp"><span>Ты успешно прошел все упражнения в этом уроке!</span></div>
            <div className="buttonp" onClick={(e) => window.location.href = "#close"}><span>К следующему уроку</span></div>
            
            
          </div>
        </div>

        <div className="app" id="game">
          <div className="helper">
            <div className="back" onClick={(e) => window.location.href = "#close"}></div>
            <div className="progress"><Status /></div>
            <div className="resetgame" onClick={(e) => this.props.restartTutorial()}>

            </div>
            <div className="helpertext">
              <Helper lesson={this.props.tutorialState.lesson} mode={this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].vmode} />
            </div>



          </div>
          <div className="exercise">
            <Hint />
            <div className="eyes"></div>
            <Exercise />
          </div>
              <div className="abacusholder" 
              id="abacusholder" >
                <Abacus />
              </div>
          
        </div>
      </>
    )
  }
}


export default Tutorial

