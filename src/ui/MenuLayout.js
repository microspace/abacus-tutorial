import React, { useContext } from 'react'



import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

import { Icon } from '@iconify/react';
import abacusIcon from '@iconify/react/fa-solid/abacus';

import { AbacusContext } from "../contexts/AppContext";
import "./MenuLayout.css"

function MenuLayout() {

    let currentCont = useContext(AbacusContext);

    return (
        <div className="container">
            <div></div>
            <div><h1>Abacus Tutorial</h1></div>
            <div>
            <ul>
                {currentCont.state.allLevelsState.map((level, index) => (
                    <li
                        key={level.name}
                        onClick={() => currentCont.actions.selectOperation(index)}
                        selected={currentCont.state.selectedOperation === index}
                    >
                        <a href="#"><Icon icon={abacusIcon} /> {level.name}</a>

                    </li>
                ))}

            </ul>
            </div>
            <ul>
                {currentCont.state.allLevelsState[currentCont.state.selectedOperation].sublevels.map((level, index) => {
                    let percentage = Math.floor((level.currentExercise - 1) / (level.exerciseCount) * 100);
                    return (
                        <li key={level.name} onClick={(e) => currentCont.actions.startGame(currentCont.state.selectedOperation, index)}>
                            <a href="#"><div style={{ width: 50, height: 50 }}><CircularProgressbar value={percentage} text={`${percentage}%`} /></div>
                                {level.name}</a>
                        </li>
                    )
                })}
            </ul>

        </div>
    );
}

export default MenuLayout;